#include "stdint.h"

struct sync_t
{
	uint8_t frst;
	uint8_t sec;
};

struct head_t
{
	sync_t sync;
	uint8_t length;
	uint8_t id;
};

struct pack_70
{
	head_t head;
	float data[9];
	int32_t geo_shirota;
	int32_t geo_dolgota;
	float 	geo_vysota;
	uint16_t crc;
};
	