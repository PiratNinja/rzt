#include "crc16.h"

void add_CRC(unsigned short *fcs, unsigned char c)
{
	*fcs = ccitt_crc16_table[(*fcs >> 8 ^ c) & 0xffU] ^ (*fcs << 8);
}