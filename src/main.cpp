#include "stdio.h"
#include "string.h"

#include "hardware_init.h"
#include "crm100.h"
#include "BINS_protocol.h"
#include "crc16.h"

#include "lcd.h"
#include "text.h"
#include "com.h"

#include "display.h"
#include "uart.h"
#include "timer.h"

#include "MDR32F9Qx_ssp.h"
#include "MDR32F9Qx_port.h"
#include "MDR32F9Qx_rst_clk.h"
#include "MDR32F9Qx_uart.h"

#define SYS_FREQ 		64000000

static inline void crm100_enable();
static inline void crm100_disable();
static void delay(uint t);

// mnp routines
static void mnp_transaction_end();
static void mnp_timeout();

static char mnp_data[256];

// bins routines
static void bins_transaction_end();
static void bins_timeout();

static char bins_data[256];


/* uart 1 ----------------------------------------------------------------*/
class UART1 : public UART<UART1, 256>
{
public:
	static UART1 * stThis;
	inline static void static_IRQHandler() { stThis->IRQHandler(); }
public:
	UART1(MDR_UART_TypeDef * uart) : UART(uart) {}
	void set_timeout(){ mnp_timeout(); }
};
UART1 serial(MDR_UART1);
UART1 * UART1::stThis = &serial;

/* timer 1 ---------------------------------------------------------------*/
class timer1 : public timer<timer1, 500000>
{
public:
	static timer1 * stThis;
	inline static void static_IRQHandler() { stThis->IRQHandler(); }
public:
	timer1(MDR_TIMER_TypeDef * tmr) : timer(tmr) {}
	void callback() { mnp_transaction_end(); }
};

timer1 tmr1(MDR_TIMER1);
timer1 * timer1::stThis = &tmr1;

/* uart 2 ----------------------------------------------------------------*/
class UART2 : public UART<UART2, 256>
{
public:
	static UART2 * stThis;
	inline static void static_IRQHandler() { stThis->IRQHandler(); }
public:
	UART2(MDR_UART_TypeDef * uart) : UART(uart) {}
	void set_timeout(){ bins_timeout(); }
};
UART2 serial2(MDR_UART2);
UART2 * UART2::stThis = &serial2;

/* timer 2 ---------------------------------------------------------------*/
class timer2 : public timer<timer2, 500000>
{
public:
	static timer2 * stThis;
	inline static void static_IRQHandler() { stThis->IRQHandler(); }
public:
	timer2(MDR_TIMER_TypeDef * tmr) : timer(tmr) {}
	void callback() { bins_transaction_end(); }
};

timer2 tmr2(MDR_TIMER2);
timer2 * timer2::stThis = &tmr2;

display<6, 8, 128, 64> dsp;  // set font size and screen size pixels

msg_t resp, dsp_resp;

int main()
{
  Frequency_CLK_PLL(1, 7);
	MDR_RST_CLK->PER_CLOCK = 0xFFFFFFFF; 

  LCD_INIT();

	CurrentFont = &Font_6x8;
	CurrentMethod = MET_AND;
	
	spi_init();
	uart_init(MDR_UART1, MDR_PORTB, PORT_Pin_5, PORT_Pin_6, PORT_FUNC_ALTER, 9600);  // for MNP-M7
	uart_init(MDR_UART2, MDR_PORTF, PORT_Pin_1, PORT_Pin_0, PORT_FUNC_OVERRID, 115200);  // for BINS
	timer_init(MDR_TIMER1);
	timer_init(MDR_TIMER2);
	NVIC_init();
	
	serial.cnt_rcv();
	serial2.cnt_rcv();
	
	crm100_disable();
	
	while(1)
	{
		/* get data from crm100 ------------------------------------------*/
		crm100_enable();
		/* cmd msg: rrs - SPI, cbit - disable, rr - 150 percent */
		resp = get_stt_msg(MDR_SSP2, set_cmd_msg(true, false, 0x2));
		for(int i = 0xfff; i != 0; i--);
		crm100_disable();
		if(resp.checksum == get_checksum(resp)) 
			dsp_resp = resp;
		
		/* get data from MNP ---------------------------------------------*/
		char  gnrmc_str[64];
		char  crdns[64] = {0};
		char * gnrmc = strstr(mnp_data, "GNRMC");
		if(gnrmc)
		{
			strncpy(gnrmc_str, mnp_data, sizeof(crdns));
		
			char * f = strchr(gnrmc_str, ',', 1);
			char * o = strchr(gnrmc_str, ',', 6);
			
			if(o > f)
			{
				strncpy(crdns, f+1, o - f);
				crdns[o-f] = 0;
			}
		}

		//rcv_data[0] = 0;
		
		/* get data from bins --------------------------------------------*/
		float Kren = reinterpret_cast<pack_70*>(&bins_data[0])->data[6];
		float Kurs = reinterpret_cast<pack_70*>(&bins_data[0])->data[7];
		float Tangaj = reinterpret_cast<pack_70*>(&bins_data[0])->data[8];
			
		/* scr upd -------------------------------------------------------*/
		dsp.cls();
		dsp << "crm100: ";
		dsp.new_line("   ") << dsp_resp.rate() << "/" << \
													(static_cast<int>(dsp_resp.temp_code()) - 531) / 2.75f;
		dsp.new_line("MNP-M7 (GNRMC): ");
		dsp.new_line(crdns);
//		dsp << "BINS:";
		dsp.new_line("Kren: ") << Kren;
		dsp.new_line("Kurs: ") << Kurs;
 		dsp.new_line("Tangaj: ") << Tangaj;
		/* scr upd -------------------------------------------------------*/
		
		delay(0x2ffff);
	}
	
	return 0;
}

static void mnp_transaction_end() { serial.get_rx_data(mnp_data); }
static void mnp_timeout() { tmr1.timeout_us(3000); }

static void bins_transaction_end() { serial2.get_rx_data(bins_data); }
static void bins_timeout() { tmr2.timeout_us(1000); }

static inline void crm100_enable()
{
	PORT_ResetBits(MDR_PORTD, PORT_Pin_7);
	PORT_SetBits(MDR_PORTB, PORT_Pin_0);
}
static inline void crm100_disable() 
{ 
	PORT_SetBits(MDR_PORTD, PORT_Pin_7);
	PORT_ResetBits(MDR_PORTB, PORT_Pin_0);
}

static void delay(uint to)
{
	uint ticks = SYS_FREQ/1000000/15 * to;
	do { } while(ticks--);
}

/* interrupts *-----------------------------------------------------------*/
extern "C" {
	void UART1_IRQHandler(void) { UART1::static_IRQHandler(); }
	void UART2_IRQHandler(void) { UART2::static_IRQHandler(); }
	void Timer1_IRQHandler(void) { timer1::static_IRQHandler(); }
	void Timer2_IRQHandler(void) { timer2::static_IRQHandler(); }
}
/* -----------------------------------------------------------------------*/
